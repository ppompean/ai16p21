package protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Cette classe assure la réception et l'envoi de données sur les flux entrants et sortants passés en paramètres.
 */
public class Protocol {

    /**
     * Permet de récupérer un Packet sur le flux entrant
     * @param i le DataInputStream dont on vont extraire le Packet
     * @return le Packet extrait
     * @throws IOException en cas d'erreur sur le flux
     */
    public static Packet parseMessage(DataInputStream i) throws IOException {
        ProtocolType pType = ProtocolType.fromValue(i.readByte());
        String message = i.readUTF();
        return new Packet(pType, message);
    }

    /**
     * Envoie un Packet sur le flux sortant
     * @param pack le Packet à envoyer
     * @param out le flux sortant sur lequel transmettre l'information
     * @throws IOException en cas d'erreur sur le flux
     */
    public static void sendPacket(Packet pack, DataOutputStream out) throws IOException {
        out.writeByte(pack.getType().getValue());
        out.writeUTF(pack.getContent());
    }
}
