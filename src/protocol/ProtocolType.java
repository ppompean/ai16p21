package protocol;

/**
 * Enumération des différents type de messages protocolaire
 */
public enum ProtocolType {
    HELLO((byte) 0),
    MESSAGE((byte) 1),
    NICK((byte) 2),
    QUIT((byte) 3),
    ERROR((byte) 42);

    /**
     * La valeur du type
     */
    private byte value;

    ProtocolType(byte value) {
        this.value = value;
    }

    /**
     * Renvoie le type associé au byte passé en paramètre
     * @param value la valeur en byte à faire correspondre
     * @return le type associé, le type ERROR sinon.
     */
    public static ProtocolType fromValue(byte value) {
        for (ProtocolType v : values()){
            if (value == v.value) {
                return v;
            }
        }
        return ERROR;
    }

    /**
     * Getter de la valeur
     * @return la valeur
     */
    public byte getValue() {
        return value;
    }
}
