package protocol;

/**
 * La classe Packet permet d'encapsuler les données qui transites entre les différentes sockets
 */
public class Packet {

    /**
     * Type basé sur l'énumération 'ProtocolType' qui définit le type du Packet
     */
    private ProtocolType type;

    /**
     * Champ stockant le contenu du paquet (texte dans le cas d'un message par exemple)
     */
    private String content;

    /**
     * Instancie un nouveau Packet
     * @param type Type de Packet créé
     * @param content Contenu du Packet
     */
    public Packet(ProtocolType type, String content) {
        this.type = type;
        this.content = content;
    }

    /**
     * Défini ou met à jour le contenu du Packet
     * @param content le nouveau contenu du Packet
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Getter du champ 'type'
     * @return la valeur du champ 'type'
     */
    public ProtocolType getType(){
        return this.type;
    }

    /**
     * Getter du champ 'content'
     * @return la valeur du champ 'content'
     */
    public String getContent(){
        return this.content;
    }

    /**
     * Renvoie la chaîne de caractères formattée associée à la classe
     * @return la chaîne formattée décrivant l'objet
     */
    public String toString() {
        return "Packet [" + this.type.toString() + "]: \"" + this.content + "\"";
    }
}
