package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

import protocol.Packet;
import protocol.Protocol;
import protocol.ProtocolType;

/**
 * Classe principale du client (point d'entrée)
 */
public class MainClient {

    /**
     * Détermine si le client à reçu un "HELLO" ou non
     */
    private static boolean receivedHello = false;

    /**
     * Détermine si le client désire quitter
     */
    private static boolean notQuit = true;

    public static void setReceivedHello() { receivedHello = true; }
    public static boolean getNotQuit() { return notQuit; }
    public static void setQuit() { notQuit = false; }

    public static void main(String[] args) {
        int port = 10800;
        Socket socket = null;
        ClientListener clientListener = null;
        Scanner scanner = null;
        try {
            socket = new Socket("localhost", port);
            System.out.println(TerminalColors.GREEN + "Connected to " + port + TerminalColors.RESET);
            OutputStream out = socket.getOutputStream();
            InputStream in = socket.getInputStream();
            DataOutputStream dos = new DataOutputStream(out);

            // Démarre un nouveau Thread client pour l'écoute de messages
            DataInputStream dis = new DataInputStream(in);
            clientListener = new ClientListener(dis);
            clientListener.start();

            // Un Scanner permettant de récupérer les messages entrés dans la console par l'utilisateur
            scanner = new Scanner(System.in);

            // Envoie un "HELLO" au serveur
            Protocol.sendPacket(new Packet(ProtocolType.HELLO, ""), dos);

            // Attend une réponse "HELLO du server"
            Thread.sleep(500);
            while(!receivedHello) {
                System.out.println(TerminalColors.RED + "Connection not established. Can't send message!" + TerminalColors.RESET);
                Thread.sleep(500);
            }

            // Lecture des messages tant que l'utilisateur ne demande pas à quitter
            while(notQuit) {
                String command;
                if(!scanner.hasNextLine()) {
                    notQuit = false;
                    command = "/quit";
                } else {
                    command = scanner.nextLine();
                }
                if (command.isBlank()) {
                    // Do nothing. Sending blank messages is a threat to online communication.
                    // Who wants to receive a blank message? I certainly don't.
                } else if(command.startsWith("/nick ")) {
                    String nickname = command.substring(6).strip();
                    if(nickname.isEmpty()) {
                        System.out.println(TerminalColors.RED + "No nickname supplied!");
                        System.out.println("run: /nick YOURNICKNAME" + TerminalColors.RESET);
                    } else {
                        Protocol.sendPacket(new Packet(ProtocolType.NICK, nickname), dos);
                    }
                } else if(command.startsWith("/quit")) {
                    System.out.println(TerminalColors.RED + "Goodbye!" + TerminalColors.RESET);
                    notQuit = false;
                    Protocol.sendPacket(new Packet(ProtocolType.QUIT, "Goodbye from client"), dos);
                } else { // Send a message
                    Protocol.sendPacket(new Packet(ProtocolType.MESSAGE, command), dos);
                }
            }
        } catch (IOException ex) {
            System.err.println(TerminalColors.RED + "MainClient: Socket error" + TerminalColors.RESET);
        } catch (InterruptedException ex) {
            System.err.println(TerminalColors.RED + "MainClient: InterruptedException" + TerminalColors.RESET);
        }
        // Close the socket
        try {
            if(socket != null) {
                socket.close();
            }
        } catch (IOException ex){}
        // Close the scanner
            if(scanner != null) {
                scanner.close();
            }
        // Kill the second thread
        if(clientListener != null) {
            if(clientListener.isInterrupted()) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex){}
                clientListener.interrupt();
            }
        }
    }
}
