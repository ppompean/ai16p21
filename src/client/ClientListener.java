package client;

import java.io.DataInputStream;
import java.io.IOException;

import protocol.Packet;
import protocol.Protocol;

public class ClientListener extends Thread {

    /**
     * Le flux de donnée entrant pour y passer des données
     */
    private DataInputStream dis;

    public ClientListener(DataInputStream dis) {
        this.dis = dis;
    }

    @Override
    public void run() {
        try {
            while(MainClient.getNotQuit()) {
                Packet packet = Protocol.parseMessage(dis);
                // Switch case pour les différents type de Packet
                switch (packet.getType()){
                    case HELLO:
                        MainClient.setReceivedHello();
                        System.out.println(TerminalColors.GREEN + "Connection established :)" + TerminalColors.RESET);
                        break;
                    case MESSAGE:
                        System.out.println(TerminalColors.YELLOW + packet.getContent() + TerminalColors.RESET);
                        break;
                    case ERROR:
                        System.out.println(TerminalColors.RED + "Error from server: " + packet.getContent() + TerminalColors.RESET);
                        break;
                    case QUIT:
                        MainClient.setQuit();
                        break;
                    default:
                        System.out.println(TerminalColors.RED
                                + "Unrecognized message from server! What should I do? code: "
                                + packet.getType().getValue()
                                + ", message: "
                                + packet.getContent()
                                + TerminalColors.RESET);
                }
            }
        } catch (IOException e) {
            // Handled by the other thread.
        }
    }
}
