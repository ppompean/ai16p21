package server;

import protocol.Packet;
import protocol.Protocol;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Classe principale du serveur (point d'entrée)
 */
public class MainServer {
    private static ArrayList<ClientConnection> chaussettes;
    private static int port;

    public static void main(String[] args) {
        port = 10800;
        chaussettes = new ArrayList<ClientConnection>();
        ServerSocket ssocket;
        try {
            ssocket = new ServerSocket(port);
            System.out.println("Now listening to " + port + ". Ready to accept connections.");

            // Attente en boucle de connexions entrantes
            while (true) {
                Socket threadSocket = ssocket.accept();
                ClientConnection c = new ClientConnection(threadSocket);
                chaussettes.add(c);

                // Création et lancement d'un nouveau Thread
                ServerThread t = new ServerThread(c);
                t.start();
            }
        } catch (IOException ex) {
            System.err.println("MainServer: Server Socket error");
        }
    }

    /**
     * Supprime de la liste des connexions clientes un connexion donnée en paramètre
     * @param c la connexion à supprimer
     * @return vrai si la suppression est effective, faux sinon
     */
    public static boolean removeConnection(ClientConnection c) {
        return chaussettes.remove(c);
    }

    /**
     * Transfère un paquet à tous les clients
     * @param pak le Packet à transférer
     * @throws IOException en cas d'erreur sur le flux
     */
    public static void broadcastPacket(Packet pak) throws IOException {
        for (ClientConnection clientConn : chaussettes) {
            if (!clientConn.getUsername().equals("")) {
                DataOutputStream socketOutStream = clientConn.getDataOutputStream();
                Protocol.sendPacket(pak, socketOutStream);
            }
        }
    }

    /**
     * Vérifie l'existance d'un nom d'utilisateur parmi les connexions clientes
     * @param username le nom d'utilisateur
     * @return vrai si l'utilisateur existe, faux sinon
     */
    public static boolean usernameExists(String username) {
        return chaussettes.stream().anyMatch(c -> c.getUsername().equals(username));
    }
}
