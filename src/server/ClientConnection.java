package server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.text.MessageFormat;

/**
 * Classe représentant une connexion client, encapsulant une socket, un flux de donnée sortant et un nom d'utilisateur
 */
public class ClientConnection {
    private Socket socket;
    private String username;
    private DataOutputStream dos;

    /**
     * Constructeur avec paramètre Socket d'une instance
     * @param socket la Socket associé à l'object ClientConnection
     * @throws IOException en cas d'erreur sur le flux extrait
     */
    public ClientConnection(Socket socket) throws IOException {
        this.socket = socket;
        username = "";
        dos = new DataOutputStream(socket.getOutputStream());
    }

    /**
     * Getter du champ 'socket'
     * @return la valeur du champ 'socket'
     */
    public Socket getSocket() {
        return this.socket;
    }

    /**
     * Getter du champ 'username'
     * @return la valeur du champ 'username'
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Défini le nom d'utilisateur s'il n'a pas déjà été défini.
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter du champ 'dos'
     * @return la valeur du champ 'dos'
     */
    public DataOutputStream getDataOutputStream() {
        return dos;
    }

    /**
     * Renvoie la chaîne de caractères formattée associée à la classe
     * @return la chaîne formattée décrivant l'objet
     */
    public String toString() {
        return MessageFormat.format("ClientConnection: [ip {0}] [nickname {1}]", this.socket.getInetAddress(), this.username);
    }
}
