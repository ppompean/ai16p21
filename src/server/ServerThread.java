package server;

import exceptions.IlAPasDitBonjourException;
import protocol.Packet;
import protocol.Protocol;
import protocol.ProtocolType;

import java.io.*;
import java.text.MessageFormat;

public class ServerThread extends Thread {

    /**
     * La connexion client associée à l'instance
     */
    private ClientConnection myConnection;

    public ServerThread(ClientConnection chaussette) {
        this.myConnection = chaussette;
    }

    @Override
    public void run() {
        try {
            OutputStream out = this.myConnection.getSocket().getOutputStream();
            InputStream in = this.myConnection.getSocket().getInputStream();

            DataInputStream dis = new DataInputStream(in);
            DataOutputStream dos = new DataOutputStream(out);

            System.out.println("New client on ip " + this.myConnection.getSocket().getInetAddress().toString());

            // Lecture du premier Packet reçu, qui doit correspondre au type "Hello"

            Packet pak;
            pak = Protocol.parseMessage(dis);

            if(pak.getType().equals(ProtocolType.HELLO)) {
                // Renvoie un "Hello" si tout est ok
                Protocol.sendPacket(new Packet(ProtocolType.HELLO, "hello from server"), dos);
            } else {
                // Throw une erreur sinon
                throw new IlAPasDitBonjourException();
            }

            // Lecture des messages tant que l'utilisateur ne demande pas à quitter

            boolean notQuit = true;
            while(notQuit) {
                pak = Protocol.parseMessage(dis);
                System.out.println("Received packet " + pak.toString());
                switch (pak.getType()){
                    case MESSAGE:
                        // On vérifie que l'utilisateur à un pseudo
                        if(myConnection.getUsername().equals("")) {
                            Protocol.sendPacket(new Packet(ProtocolType.ERROR, "You didn't set a nickname.\nrun: /nick YOURNICKNAME"), dos);
                            break;
                        }
                        pak.setContent(myConnection.getUsername() + " said : " + pak.getContent());
                        // Transfert du Packet à tous les autres clients
                        MainServer.broadcastPacket(pak);
                        break;
                    case NICK:
                        String content = pak.getContent();
                        // Vérification du format du pseudo
                        if (content.matches("(?i)^(?:(?![×Þß÷þø])[-'0-9a-zÀ-ÿ_])+$")) {
                            // Vérification des homonymes
                            if (MainServer.usernameExists(content)) {
                                Protocol.sendPacket(new Packet(ProtocolType.ERROR, "This nickname already exists, sorry you were not the first !"), dos);
                            } else {
                                myConnection.setUsername(content);
                                // Transfert du Packet à tous les autres clients
                                MainServer.broadcastPacket(new Packet(ProtocolType.MESSAGE,
                                    MessageFormat.format("{0} joined the chat.", myConnection.getUsername())));
                            }
                        } else {
                            // Renvoie une erreur si le format n'est pas bon
                            Protocol.sendPacket(new Packet(ProtocolType.ERROR, "You didn't set a correct nickname."), dos);
                        }
                        break;
                    case QUIT:
                        notQuit = false;
                        break;
                    case ERROR:
                        System.err.println(MessageFormat.format("Error from {0} : {1}",
                                myConnection.getSocket(),
                                pak.getContent()));
                        break;
                    default:
                        Protocol.sendPacket(new Packet(ProtocolType.ERROR, "not implemented"), dos);
                }
            }
        } catch (IOException e) {
            System.err.println(MessageFormat.format("Client connection interrupted. Nick: {0}, ip: {1}", myConnection.getUsername(), myConnection.getSocket().getInetAddress()));
        } catch (IlAPasDitBonjourException e) {
            System.err.println(MessageFormat.format("Il a pas dit bonjour {0} !", myConnection.getSocket().getInetAddress()));
        } finally {
            MainServer.removeConnection(this.myConnection);
            try {
                MainServer.broadcastPacket(new Packet(ProtocolType.MESSAGE, MessageFormat.format("{0} left the chat.", myConnection.getUsername())));
            } catch (IOException e) {
                System.err.println(MessageFormat.format("Error: Can't broadcast message that {0} left the chat", myConnection.getUsername()));
            }
            try {
                myConnection.getSocket().close();
            } catch (IOException e) {
                System.err.println("Can't close connection");
            }
        }
    }
}
